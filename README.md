# Projet stage (Mosaique)

## 1

* creation d'un envernemnet 
```
python3 -m vnev env
```
* pour l'activer 
pour linux
```
source env/bin/activate
```
pour windos
```
source env/Scripts/activtate
```
* puis il faut installer Django
```
pip install django
```
* create le fichier reqiurements.txt pour stocker les packeges installées
```
pip freeze > requirements.txt
```

* sans oublier l'activation de l'envernement on cree notre projet
```
django-admin startproject nameProject .
```
* pour tester on lance le server avec 
```
python manage.py runserver
```
* aller sur le navigateur
```
localhost:8000
```
* à chaque fois que on veut lancer le server il faut activer l'envernement, puis lancer le server et aller sur le navigateur

---

## 2    

* creation de projet 
```
django-admin startproject Mosaique
```
* creation d'un route pour la page d'acceuil, dans le fichier urls.py de Mosaique on ajoute
```python
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'), # ce que on a jouter avec le fichier views.py
]
```
* on cree un docier templates dans Mosaique, avec la les ficheir `base.html et home.html`
* dans le fichier views.py de Mosaique on ajoute
```python
from django.shortcuts import render

def home(request):
    return render(request, 'home.html')
```
* dans le fichier sttings.py de mosaique on ajoute :
```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')], # ce que on a ajouter
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```
* creation du dossier static dans Mosaique, pour ajouter à l'interieur les fichiers `css, js, images, ...`
* modification du fichier settings.py de Mosaique, pour trouver notre dossier static
```python
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'mosaique/static') 
]
```
* on lance le commande 
```
python manage.py collectstatic
```
cette commande pour collecter les fichiers static dans le dossier static_root

* sans oublier de loader les fichiers static dans le fichier base.html
```html
{% load static %}
```

* création d'un superuser
```
python3 manage.py createsuperuser
```

## Catégorie model

* faire un application pour les catégories
```
python3 manger.py startapp category
```
* l'ajouter dans la ficher settings.py de Mosaique
```python
INSTALLED_APPS = [
    'mosaique.apps.MosaiqueConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages', 
    'django.contrib.staticfiles',
    'category', # ce que on a ajouter
]
```

* dans le fichier models.py de category on ajoute
```python
# Create your models here.

class Category(models.Model):
    category_name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    description = models.TextField(max_length=500, blank=True)
    cat_image = models.ImageField(upload_to='photos/categories', blank=True)

    class Meta:
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.category_name
```

* on ajoute dans le fichier admin.py de category
```python
from .models import Category

# Register your models here.
admin.site.register(Category)
```

* faire les migrations
```
python3 manger.py makemigrations
python3 manger.py migrate
```


## utilisateur model

* faire un application pour les utilisateurs
```
python3 manger.py startapp accounts
```

* l'ajouter dans la ficher settings.py de Mosaique
```python
INSTALLED_APPS = [
    'mosaique.apps.MosaiqueConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages', 
    'django.contrib.staticfiles',
    'category',
    'accounts',
]
```

* dans le fichier models.py de accounts on ajoute
```python
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.

class User(AbstractBaseUser):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    username = models.CharField(max_length=50, unique=True)
    email_address = models.EmailField(max_length=100, unique=True)
    phone_number = models.CharField(max_length=50)


    # required
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email_address'


class UserManager(BaseUserManager):
    def create_user(self, first_name, last_name, username, email_address, phone_number, password=None):
        if not email_address:
            raise ValueError("L'utilisateur doit avoir une adresse e-mail")
        if not username:
            raise ValueError("L'utilisateur doit avoir un nom d'utilisateur")
        if not first_name:
            raise ValueError("L'utilisateur doit avoir un prénom")
        if not last_name:
            raise ValueError("L'utilisateur doit avoir un nom de famille")
        if not phone_number:
            raise ValueError("L'utilisateur doit avoir un numéro de téléphone")
        
        user = self.model(
            email_address=self.normalize_email(email_address),
            username=username,
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number
        )

        user.set_password(password)

        user.save(using=self._db)
        
        return user
    
    

    def create_superuser(self, first_name, last_name, username, email_address, phone_number, password):
        user = self.create_user(
            email_address=self.normalize_email(email_address),
            username=username,
            password=password,
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number
        )

        user.is_admin = True
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)

```

* on ajoute dans le fichier settings.py de Mosaique
```python
AUTH_USER_MODEL = 'accounts.User'
```

* on ajoute dans le fichier admin.py de accounts
```python
from django.contrib import admin
from .models import User

# Register your models here.
admin.site.register(User)

```

* faire les migrations
```
python3 manger.py makemigrations
python3 manger.py migrate
```








