from django.shortcuts import render, redirect
from django.contrib import messages, auth
from store.models import Product
from supplier.models import Supplier
from .models import Category
from django.contrib.auth.decorators import login_required


@login_required(login_url='login')
def create(request):
    if not request.user.is_superuser:
        messages.success(request, 'Opps :>')
        return redirect('login')
    if request.method == 'POST':
        category_name = request.POST.get('category_name')
        description = request.POST.get('category_description')

        category = Category.objects.create(
            category_name = category_name,
            description = description,
        )
        if request.FILES.get('category_image'):
            img = request.FILES.get('category_image')
            category.cat_image = img
            category.save()
        messages.success(request, 'La catégorie a été créée avec succès')
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='login')
def update(request, slug):
    category = Category.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not category:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        category.category_name = request.POST.get('category_name')
        category.description = request.POST.get('description')
        category.save()
        messages.success(request, 'La catégorie a été modifiée avec succès')
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='login')
def delete(request, slug):
    category = Category.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not delete:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    category.delete()
    messages.success(request, 'Deleted')
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='login')
def show(request, slug):
    category = Category.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not category:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    products = Product.objects.filter(category = category).all()
    return render(request, 'supplier/show.html', {'category':category ,'products':products})


@login_required(login_url='login')
def list_products(request, slug):
    category = Category.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not delete:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    products = Product.objects.filter(category = category).all()
    return render(request, 'admincos/list/products.html', {'category':category ,'products':products})

@login_required(login_url='login')
def add_products(request):
    if not request.user.is_superuser:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        product_name = request.POST.get('product_name')
        description = request.POST.get('description')
        price = request.POST.get('price')
        stock = request.POST.get('stock')
        category_id = request.POST.get('category')
        supplier_id = request.POST.get('supplier')
        category = Category.objects.filter(id = category_id).first()
        supplier = Supplier.objects.filter(id = supplier_id).first()
        if not category or not supplier:
            messages.success(request, 'Opps :>')
            return redirect(request.META.get('HTTP_REFERER'))
        product = Product.objects.create(
            product_name = product_name,
            description = description,
            price = price,
            stock = stock,
            category = category,
            supplier = supplier,
        )
        if request.FILES.get('product_image'):
            img = request.FILES.get('product_image')
            product.images = img
            product.save()

        return redirect(request.META.get('HTTP_REFERER'))

    messages.success(request, 'Opps :>')
    return redirect(request.META.get('HTTP_REFERER'))




@login_required(login_url='login')
def delete_products(request, prod):
    product = Product.objects.filter(slug = prod).first()
    if not request.user.is_superuser or not product:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    product.delete()
    messages.success(request, 'Deleted')
    return redirect(request.META.get('HTTP_REFERER'))





@login_required(login_url='login')
def update_prod(request, prod):
    product = Product.objects.filter(slug = prod).first()
    if not request.user.is_superuser or not product:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        product_name = request.POST.get('product_name')
        description = request.POST.get('description')
        price = request.POST.get('price')
        stock = request.POST.get('stock')
        category_id = request.POST.get('category')
        supplier_id = request.POST.get('supplier')
        category = Category.objects.filter(id = category_id).first()
        supplier = Supplier.objects.filter(id = supplier_id).first()

        product.product_name = product_name
        product.description = description
        product.price = price
        product.stock = stock
        product.category = category
        product.supplier = supplier
        product.save()
        messages.success(request, 'La catégorie a été modifiée avec succès')
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(request.META.get('HTTP_REFERER'))
