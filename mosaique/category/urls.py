from django.urls import path
from . import views

app_name='category'

urlpatterns = [
    path('create', views.create, name='create'),
    path('<slug:slug>/delete', views.delete, name='delete'),
    path('<slug:slug>/update', views.update, name='update'),
    path('<slug:slug>/show', views.show, name='show'),
    path('<slug:slug>/list/products', views.list_products, name='list_products'),
    path('add/products', views.add_products, name='add_products'),
    path('delete/products/<slug:prod>', views.delete_products, name='delete_products'),
    path('update/products/<slug:prod>', views.update_prod, name='update_prod'),
]