from django.shortcuts import render, redirect
from store.models import Product, ReviewRating


def home(request):
    if request.user.is_superuser:
        return redirect('admincos:controlepanel')
    products = Product.objects.all().filter(is_available=True).order_by('created_date')

    for product in products:
        reviews = ReviewRating.objects.filter(product_id=product.id, status=True)

    context = {
        'products': products,
        'reviews': reviews,
    }

    return render(request, 'accueil.html', context)