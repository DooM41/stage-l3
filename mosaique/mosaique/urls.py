from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf.urls.static import static
from django.conf import settings
from admincos.views import deneratePdf

urlpatterns = [
    path('securelogin/', admin.site.urls),
    path('', views.home, name="home"),
    path('deneratePdf', deneratePdf, name="deneratePdf"),
    path('store/', include('store.urls')),
    path('cart/', include('carts.urls')),
    path('accounts/', include('accounts.urls')),
    path('admin-cos/', include('admincos.urls')),
    path('supplier/', include('supplier.urls')),
    path('category/', include('category.urls')),
    path('orders/', include('orders.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
