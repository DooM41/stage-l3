from django.shortcuts import render, get_object_or_404, redirect
from .models import Product, ReviewRating
from category.models import Category

from .forms import ReviewForm
from django.contrib import messages
from carts.views import _cart_id
from carts.models import CartItem
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q




# Create your views here.

def store(request, category_slug = None):
    categories = None
    products = None

    if category_slug != None:
        categories = get_object_or_404(Category, slug=category_slug)
        products = Product.objects.filter(category=categories, is_available=True)
        paginator = Paginator(products, 15)
        page = request.GET.get('page')
        paged_products = paginator.get_page(page)
        products_count = products.count()
    else:
        products = Product.objects.all().filter(is_available=True).order_by('id')
        paginator = Paginator(products, 15)
        page = request.GET.get('page')
        paged_products = paginator.get_page(page)
        products_count = products.count()

    context = {
        'products': paged_products,
        'products_count': products_count,
    }

    return render(request, 'store/store.html', context)


def product_detail(request, category_slug, product_slug):
    try:
        single_product = Product.objects.get(category__slug=category_slug, slug=product_slug)
        in_cart = CartItem.objects.filter(cart__cart_id=_cart_id(request), product=single_product).exists()

    except Exception as e:
        raise e

    reviews = ReviewRating.objects.filter(product_id=single_product.id, status=True)
    print(reviews)

    context = {
        'single_product': single_product,
        'in_cart': in_cart,
        'reviews': reviews,
    }
    return render(request, 'store/product_detail.html', context)


def submit_review(request, product_id):
    url = request.META.get('HTTP_REFERER') # get the last url
    if request.method == 'POST':
        try:
            reviews = ReviewRating.objects.get(user__id=request.user.id, product__id=product_id)
            form = ReviewForm(request.POST, instance=reviews)
            form.save()
            messages.success(request, "Merci! Votre avis a été mis à jour.")
            return redirect(url)
        except ReviewRating.DoesNotExist:
            form = ReviewForm(request.POST)
            if form.is_valid():
                data = ReviewRating()
                data.subject = form.cleaned_data['subject']
                data.review = form.cleaned_data['review']
                data.rating = form.cleaned_data['rating']
                data.ip = request.META.get('REMOTE_ADDR')
                data.product_id = product_id
                data.user_id = request.user.id
                data.save()
                messages.success(request, "Merci! Votre avis a été soumis.")
                return redirect(url)


def get_products_with_q(request):
    q = request.GET.get('q')
    if q !='':
        products = Product.objects.filter(
            Q(product_name__icontains=q) |
            Q(description__icontains=q) |
            Q(price__icontains=q) |
            Q(category__category_name__icontains=q) |
            Q(compnay__icontains=q)).distinct().filter(is_available=True).all()
        # products = Product.objects.filter(is_available=True).order_by('id')
        print(products)
        paginator = Paginator(products, 15)
        page = request.GET.get('page')
        paged_products = paginator.get_page(page)
        products_count = products.count()

        context = {
            'products': paged_products,
            'products_count': products_count,
            }

    return render(request, 'store/store.html', context)