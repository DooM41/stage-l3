from django.db import models
from category.models import Category
from django.urls import reverse
from supplier.models import Supplier
from django.template.defaultfilters import slugify
from accounts.models import User

from django.db.models import Avg, Count

# Create your models here.


class Product(models.Model):
    product_name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    description = models.TextField(max_length=500, blank=True)
    price = models.IntegerField()
    images = models.ImageField(upload_to='photos/products')
    stock = models.IntegerField()
    is_available = models.BooleanField(default=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE, related_name='product_supplier', null=True, blank=True)
    compnay = models.CharField(max_length=200, blank=True, null=True)


    def get_url(self):
        return reverse('product_detail', args=[self.category.slug, self.slug])


    def averageRaview(self):
        reviews = ReviewRating.objects.filter(product=self, status=True).aggregate(average = Avg('rating'))
        avg = 0
        if reviews['average'] is not None:
            avg = float(reviews['average'])
        return avg

    def countRaview(self):
        reviews = ReviewRating.objects.filter(product=self, status=True).aggregate(count = Count('id'))
        count = 0
        if reviews['count'] is not None:
            count = int(reviews['count'])
        return count



    def __str__(self):
        return self.product_name

    def save(self, **kwargs):
        if not self.slug:
            slug = slugify(self.product_name)
            while True:
                try:
                    prod = Product.objects.get(slug=slug)
                    if prod == self:
                        self.slug = slug
                        break
                    else:
                        slug = slug + '-other'
                except:
                    self.slug = slug
                    break
        super(Product, self).save()


class ReviewRating(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subject = models.CharField(max_length=100, blank=True)
    review = models.TextField(max_length=500, blank=True)
    rating = models.FloatField()
    ip = models.CharField(max_length=20, blank=True)
    status = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.subject


    def get_total_price(self):
        return self.product.price * self.qua



