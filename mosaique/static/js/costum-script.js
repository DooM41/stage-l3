function submitForm(e) {
  const s = e.parentElement.previousElementSibling.querySelector("form");
  s.submit();
}

function addImagesToContainer() {
  console.log("here");
  const imageInput = document.getElementById("image-input");
  const imageContainer = document.getElementById("image-container");
  console.log(imageInput);
  console.log(imageInput);
  imageContainer.classList.remove("d-none");
  imageContainer.classList.add("d-flex");

  const fileUrls = [];
  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      dev.classList.add("border");
      dev.classList.add("rounded");
      dev.classList.add("p-3");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute("type", "buttun");
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}
function handleAdminSideBar() {
  const adminSideBar = document.querySelector("#adminSideBar");
  const openCloseAdminSideBarIcon = document.querySelector(
    "#openCloseAdminSideBarIcon"
  );
  const openCloseAdminSideBarIconParent =
    openCloseAdminSideBarIcon.parentElement;
  adminSideBar.classList.toggle("active");
  if (adminSideBar.classList.contains("active")) {
    openCloseAdminSideBarIcon.classList.remove("fa-bars");
    openCloseAdminSideBarIcon.classList.add("fa-times");
    openCloseAdminSideBarIconParent.classList.add("bg-danger");
  } else {
    openCloseAdminSideBarIcon.classList.remove("fa-times");
    openCloseAdminSideBarIcon.classList.add("fa-bars");
    openCloseAdminSideBarIconParent.classList.remove("bg-danger");
  }
}

function handleChangeSubMenu() {
  const parents = document.querySelectorAll(
    "#adminSideBar #menu-list li.menu-list-item"
  );
  parents.forEach((parent) => {
    parent.addEventListener("click", (e) => {
      const allSubMenus = document.querySelectorAll(".sub.menu-list");
      allSubMenus.forEach((subMenu) => {
        subMenu.classList.add("d-none");
      });
      const subMenu = document.querySelector(
        `#adminSideBar .center #${parent.dataset.submenu}`
      );
      subMenu.classList.remove("d-none");
    });
  });
}
function closeAdminSideBar(btn) {
  handleAdminSideBar();
}

function handleAdminQuickAccess() {
  const menu = document.querySelector("#admin-quick-access-menu");
  const btnIcon = document.querySelector("#admin-quick-access-icon");
  menu.classList.toggle("active");
  if (btnIcon.classList.contains("fa-bars")) {
    btnIcon.classList.remove("fa-bars");
    btnIcon.classList.add("fa-times");
  } else {
    btnIcon.classList.remove("fa-times");
    btnIcon.classList.add("fa-bars");
  }
}
