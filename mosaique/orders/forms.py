from django import forms
from .models import Order


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('first_name', 'last_name', 'phone_number', 'email_address', 'address_line_1', 'country', 'city', 'postal_code', 'order_note', 'date_recovery', 'time_recovery') 