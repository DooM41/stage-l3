from django.db import models
from accounts.models import User
from store.models import Product
from django.utils import timezone

# Create your models here.


class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    payment_id = models.CharField(max_length=100)
    payment_method = models.CharField(max_length=100)
    amount_paid = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.payment_id


class Order(models.Model):
    STATUS = (
        ('Nouvelle commande', 'Nouvelle commande'),
        ('En cours de traitement', 'En cours de traitement'),
        ('Commande prête', 'Commande prête'),
        ('Commande récupéré', 'Commande récupéré'),
        ('Commande annulée', 'Commande annulée'),
    )

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    payment = models.ForeignKey(Payment, on_delete=models.SET_NULL, blank=True, null=True)
    order_number = models.CharField(max_length=20)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=50)
    email_address = models.EmailField(max_length=50)
    date_recovery = models.DateField()
    time_recovery = models.TimeField()
    address_line_1 = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    postal_code = models.CharField(max_length=50)
    order_note = models.CharField(max_length=100, blank=True)
    order_total = models.FloatField()
    tax = models.FloatField()
    status = models.CharField(max_length=50, default='Nouvelle commande', choices=STATUS)
    ip = models.CharField(max_length=50, blank=True)
    is_ordered = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)


    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def full_address(self):
        return f"{self.address_line_1}"


    def get_is_ordered(self):
        return

    def __str__(self):
        return self.order_number

    def get_recovery_datetime(self):
        return f'Le {self.date_recovery} à {self.time_recovery}'


    def get_status(self):
        return self.status


    def get_address(self):
        return f'{self.address_line_1}, {self.city}, {self.postal_code}, {self.country}'

    def get_visibilty(self):
        return 'Visible' if self.is_active else 'Archivée'
    class Meta:
        ordering = ['-is_active', '-status', 'is_ordered', '-created_at']



    def get_status_color(self):
        if self.status == 'Nouvelle commande':
            return 'warning'
        elif self.status == 'En cours de traitement':
            return 'primary'
        elif self.status == 'Commande prête':
            return 'success'
        elif self.status == 'Commande récupéré':
            return 'info'
        elif self.status == 'Commande annulée':
            return 'danger'



class OrderProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    payment = models.ForeignKey(Payment, on_delete=models.SET_NULL, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    product_price = models.FloatField()
    ordered = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)



    def __str__(self):
        return self.product.product_name


    def get_total_price(self):
        return self.product.price * self.quantity



