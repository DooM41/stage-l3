# Generated by Django 4.1.1 on 2023-06-28 20:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'ordering': ['-status']},
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('Nouvelle commande', 'Nouvelle commande'), ('En cours de traitement', 'En cours de traitement'), ('Commande prête', 'Commande prête'), ('Commande récupéré', 'Commande récupéré'), ('Commande annulée', 'Commande annulée')], default='Nouvelle commande', max_length=50),
        ),
    ]
