# Generated by Django 4.1.1 on 2023-06-29 17:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_alter_order_options_order_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('Nouvelle commande', 'Nouvelle commande'), ('En cours de traitement', 'En cours de traitement'), ('Commande prête', 'Commande prête'), ('Commande récupéré', 'Commande récupéré'), ('Commande annulée', 'Commande annulée')], default='Nouvelle commande', max_length=50),
        ),
    ]
