from django.contrib import admin
from .models import Payment, Order, OrderProduct
# Register your models here.


class OrderProductInline(admin.ModelAdmin):
    list_display = ['product', 'quantity', 'product_price', 'ordered']
    model = OrderProduct
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    list_display = ['order_number', 'full_name', 'phone', 'email', 'city', 'order_total', 'tax', 'status', 'is_ordered', 'created_at']
    list_filter = ['status', 'is_ordered']
    search_fields = ['order_number', 'first_name', 'last_name', 'phone_number', 'email_address']
    list_per_page = 20

    def full_name(self, obj):
        return obj.first_name + ' ' + obj.last_name

    def phone(self, obj):
        return obj.phone_number

    def email(self, obj):
        return obj.email_address

    def city(self, obj):
        return obj.city

    full_name.admin_order_field = 'first_name'
    phone.admin_order_field = 'phone_number'
    email.admin_order_field = 'email_address'
    city.admin_order_field = 'city'


admin.site.register(Payment)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderProduct, OrderProductInline)
