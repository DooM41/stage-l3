from django import forms
from .models import User, UserProfile

class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Entrer le mot de passe',
        'class': 'form-control',
    }))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Confirmez le mot de passe',
        'class': 'form-control',
    }))
    class Meta:
        model = User
        fields = ['first_name','last_name', 'phone_number', 'email_address', 'password']


    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = 'Entrez votre prénom'
        self.fields['last_name'].widget.attrs['placeholder'] = 'Entrer le nom de famille'
        self.fields['phone_number'].widget.attrs['placeholder'] = 'Entrez le numéro de téléphone'
        self.fields['email_address'].widget.attrs['placeholder'] = 'Entrer l\'adresse e-mail'
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        
        if password != confirm_password:
            raise forms.ValidationError(
                "Password does not match !"
            )

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name','last_name', 'phone_number']

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

class userProfileForm(forms.ModelForm):
    profile_picture = forms.ImageField(required=False, error_messages={'invalid':("Image files only")}, widget=forms.FileInput)
    class Meta:
        model = UserProfile
        fields = ['profile_picture', 'address_line_1', 'city', 'country', 'postal_code']

    def __init__(self, *args, **kwargs):
        super(userProfileForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'