from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, first_name, last_name, username, email_address, phone_number, password=None):
        if not email_address:
            raise ValueError("L'utilisateur doit avoir une adresse e-mail")
        if not username:
            raise ValueError("L'utilisateur doit avoir un nom d'utilisateur")
        if not first_name:
            raise ValueError("L'utilisateur doit avoir un prénom")
        if not last_name:
            raise ValueError("L'utilisateur doit avoir un nom de famille")
        if not phone_number:
            raise ValueError("L'utilisateur doit avoir un numéro de téléphone")

        user = self.model(
            email_address=self.normalize_email(email_address),
            username=username,
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number
        )

        user.set_password(password)

        user.save(using=self._db)

        return user



    def create_superuser(self, first_name, last_name, username, email_address, phone_number, password):
        user = self.create_user(
            email_address=self.normalize_email(email_address),
            username=username,
            password=password,
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number
        )

        user.is_admin = True
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)




class User(AbstractBaseUser):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    username = models.CharField(max_length=50, unique=True)
    email_address = models.EmailField(max_length=100, unique=True)
    phone_number = models.CharField(max_length=50)


    # required
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email_address'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'phone_number']

    objects = UserManager()


    def __str__(self):
        return self.email_address

    def full_name(self):
        return f"{self.first_name} {self.last_name}"


    def has_perm(self, perm, obj=None):
        return self.is_admin


    def has_module_perms(self, app_label):
        return True

    def check_password(self, raw_password: str) -> bool:
        return super().check_password(raw_password)


    def set_phone_number_format(self):
        return f"{self.phone_number[:2]} {self.phone_number[2:4]} {self.phone_number[4:6]} {self.phone_number[6:8]} {self.phone_number[8:10]}"



class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address_line_1 = models.CharField(max_length=100, blank=True)
    profile_picture = models.ImageField(blank=True, null=True, upload_to='userprofile')
    city = models.CharField(max_length=100, blank=True)
    postal_code = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True)


    def __str__(self):
        return self.user.first_name

    def full_address(self):
        return f"{self.address_line_1}"


