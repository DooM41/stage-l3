from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User , UserProfile
from django.utils.html import format_html

# Register your models here.


class UserAdminConfig(UserAdmin):
    list_display = ('email_address', 'username', 'first_name', 'last_name', 'phone_number', 'date_joined' ,'is_active', 'is_staff', 'is_admin')
    list_display_links = ('email_address', 'first_name', 'last_name' )
    readonly_fields = ('date_joined', 'last_login')
    orfering = ('-date_joined',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

class UserProfileAdmin(admin.ModelAdmin):
    def thumbnail(self, object):
        return format_html('<img src="{}" width="30" style="border-radius:50px" />'.format(object.profile_picture.url))
    thumbnail.short_description = 'Profile Picture'
    list_display = ('thumbnail' ,'user', 'city', 'postal_code')
    

admin.site.register(User, UserAdminConfig)
admin.site.register(UserProfile, UserProfileAdmin)