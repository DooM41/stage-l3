from django.shortcuts import render, redirect, get_object_or_404
from .forms import RegistrationForm, UserForm, userProfileForm
from .models import User, UserProfile
from django.contrib import messages, auth
from store.models import Product
from django.contrib.auth.decorators import login_required
from orders.models import Order, OrderProduct


# EMAIL VERIFICATION
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding  import force_bytes
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMessage, send_mail
from django.conf import settings
from django.http import HttpResponse


from carts.models import Cart, CartItem
from carts.views import _cart_id
import requests


# Create your views here.

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            phone_number = form.cleaned_data['phone_number']
            email_address = form.cleaned_data['email_address']
            passwrod = form.cleaned_data['password']
            username = email_address.split("@")[0]
            user = User.objects.create_user(first_name = first_name, last_name = last_name, email_address = email_address, username = username, password = passwrod, phone_number= phone_number )
            user.save()

            # USER ACTIVATION
            current_site = get_current_site(request)
            mail_subject = 'Veuillez activer votre compte'
            message = render_to_string('accounts/account_verification_email.html', {
                'user': user,
                'domain': current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user),
            })
            to_email = email_address
            send_email = EmailMessage(mail_subject, message, to=[to_email])
            send_email.send()

            #messages.success(request, 'Un e-mail de vérification a été envoyé à votre adresse e-mail')
            return redirect('/accounts/login/?command=verification&email='+email_address)
    else:
        form = RegistrationForm()
    context = {
        'form': form,
    }
    return render(request, 'accounts/register.html', context)





def login(request):
    if request.method == 'POST':
        email_address = request.POST['email_address']
        password = request.POST['password']
        try:
            utilisateur = User.objects.get(email_address=email_address)
        except:
            messages.error(request, "L'utilisateur n'existe pas")
            return redirect('login')
        if utilisateur.check_password(password):
            user = auth.authenticate(email_address=email_address, password=password)
            try:
                cart = Cart.objects.get(cart_id=_cart_id(request))
                is_cart_item_exists = CartItem.objects.filter(cart=cart).exists()
                if is_cart_item_exists:

                    cart_item = CartItem.objects.filter(cart=cart)
                    uesr_cart_item = CartItem.objects.filter(user = user)


                    product_item = []
                    product_item_user = []
                    product_item_user_id = []

                    for item in cart_item:
                        product_item.append(item.product)

                    for item in uesr_cart_item:
                        product_item_user.append(item.product)
                        product_item_user_id.append(item.id)


                    for pr in product_item:
                        if pr in product_item_user:
                            index = product_item_user.index(pr)
                            item_user = CartItem.objects.get(id = product_item_user_id[index])
                            item_user.quantity +=1
                            item_user.user = user
                            item_user.save()
                        else:
                            item = CartItem.objects.get(product=pr)
                            item.user = user
                            item.save()

            except:
                pass
            auth.login(request, user)
            messages.success(request, "Vous êtes connecté")
            url = request.META.get('HTTP_REFERER')
            try:
                query = requests.utils.urlparse(url).query
                params = dict(x.split('=') for x in query.split('&'))
                if 'next' in params:
                    nextPage = params['next']
                    return redirect(nextPage)
            except:
                return redirect('dashboard')
        else:
            messages.error(request, "Mot de passe incorrect")
            return redirect('login')
    return render(request,'accounts/login.html')





@login_required(login_url='login')
def logout(request):

    auth.logout(request)
    products = Product.objects.all().filter(is_available=True)

    context = {
        'products': products,
    }
    messages.success(request, "Vous êtes déconnecté")

    return render(request, 'accueil.html', context)



def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(request, 'Votre compte a été activé avec succès')
        return redirect('login')
    else:
        messages.error(request, 'Lien d\'activation invalide')
        return redirect('register')



@login_required(login_url='login')
def dashboard(request):
    if request.user.is_admin:
        orders = Order.objects.filter(is_active=True).all()
        return render(request, 'admincos/controlepanel.html', {"orders" : orders})
    orders = Order.objects.order_by('-created_at').filter(user_id=request.user.id, is_ordered=True)
    orders_count = orders.count()
    context = {
        'orders_count': orders_count,
    }
    return render(request, 'accounts/dashboard.html', context)



def forgotpassword(request):
    if request.method == 'POST':
        email_address = request.POST['email_address']
        if User.objects.filter(email_address=email_address).exists():
            user = User.objects.get(email_address__exact=email_address)

            # RESET PASSWORD EMAIL
            current_site = get_current_site(request)
            mail_subject = 'Réinitialiser votre mot de passe'
            message = render_to_string('accounts/reset_password_email.html', {
                'user': user,
                'domain': current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user),
            })
            to_email = email_address
            send_email = EmailMessage(mail_subject, message, to=[to_email])
            send_email.send()

            messages.success(request, 'Un e-mail de réinitialisation du mot de passe a été envoyé à votre adresse e-mail')
            return redirect('login')
        else:
            messages.error(request, 'Le compte n\'existe pas')
            return redirect('forgotpassword')


    return render(request, 'accounts/forgotPassword.html')


def resetpassword_validate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None


    if user and default_token_generator.check_token(user, token):
        request.session['uid'] = uid
        messages.success(request, 'Veuillez réinitialiser votre mot de passe')
        return redirect('resetpassword')
    else:
        messages.error(request, 'Ce lien a expiré')
        return redirect('login')



def resetpassword(request):
    if request.method == 'POST':
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']

        if password == confirm_password:
            uid = request.session.get('uid')
            user = User.objects.get(pk=uid)
            user.set_password(password)
            user.save()
            messages.success(request, 'Votre mot de passe a été réinitialisé avec succès')
            return redirect('login')
        else:
            messages.error(request, 'Les mots de passe ne correspondent pas')
            return redirect('resetpassword')

    else:
        return render(request, 'accounts/resetPassword.html')

@login_required(login_url='login')
def my_orders(request):
    orders = Order.objects.filter(user=request.user, is_ordered=True).order_by('-created_at')
    context = {
        'orders': orders,
    }
    return render(request, 'accounts/my_orders.html', context)

@login_required(login_url='login')
def edit_profile(request):
    print(request.user)
    userprofile, created = UserProfile.objects.get_or_create(
        user=request.user
    )

    # userprofile = get_object_or_404(UserProfile, user=request.user)
    print('1')
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = userProfileForm(request.POST, request.FILES, instance=userprofile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Votre profil a été mis à jour')
            return redirect('edit_profile')
    else:
        user_form = UserForm(instance=request.user)
        profile_form = userProfileForm(instance=userprofile)
    context = {
        'user_form': user_form,
        'profile_form': profile_form,
        'userprofile': userprofile,
    }
    return render(request, 'accounts/edit_profile.html', context)

@login_required(login_url='login')
def change_password(request):
    if request.method == 'POST':
        current_password = request.POST['current_password']
        new_password = request.POST['new_password']
        confirm_new_password = request.POST['confirm_new_password']

        user = User.objects.get(username__exact=request.user.username)
        if new_password == confirm_new_password:
            success = user.check_password(current_password)
            if success:
                user.set_password(new_password)
                user.save()
                messages.success(request, 'Votre mot de passe a été mis à jour')
                return redirect('change_password')
            else:
                messages.error(request, 'Veuillez entrer le bon mot de passe actuel')
                return redirect('change_password')
        else:
            messages.error(request, 'Les nouveaux mots de passe ne correspondent pas')
            return redirect('change_password')

    return render(request, 'accounts/change_password.html')


@login_required(login_url='login')
def order_detail(request, order_id):
    order_detail = OrderProduct.objects.filter(order__id=order_id)
    order = Order.objects.get(id=order_id)
    sub_total = 0
    for i in order_detail:
        sub_total += i.product_price * i.quantity

    context = {
        'order_detail': order_detail,
        'order': order,
        'sub_total': sub_total,
    }
    return render(request, 'accounts/order_detail.html', context)

