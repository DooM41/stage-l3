from django.db import models
from django.template.defaultfilters import slugify

class Supplier(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    phone = models.CharField(max_length=255 , null=True, blank=True)
    email = models.EmailField(max_length=255 , null=True, blank=True)
    slug = models.SlugField(max_length=200, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    
    def save(self, *args, **kwargs):
            self.slug = slugify(self.name)
            super(Supplier, self).save(*args, **kwargs)

    def __str__(self):
        return self.name