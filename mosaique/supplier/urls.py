from django.urls import path
from . import views

app_name='supplier'

urlpatterns = [
    path('create', views.create, name='create'),
    path('<slug:slug>/delete', views.delete, name='delete'),
    path('<slug:slug>/update', views.update, name='update'),
    path('<slug:slug>/show', views.show, name='show'),
    path('<slug:slug>/products', views.list_products, name='list_products'),
]