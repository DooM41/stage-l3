from django.contrib import admin
from .models import Supplier

# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email', 'slug')
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Supplier, ProductAdmin)


