from django.shortcuts import render, redirect
from django.contrib import messages, auth
from .models import Supplier
from store.models import Product
from django.contrib.auth.decorators import login_required


@login_required(login_url='login')
def create(request):
    if not request.user.is_superuser:
        messages.success(request, 'Opps :>')
        return redirect('login')
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        supplier = Supplier.objects.create(name = name, phone = phone, email = email)
        messages.success(request, 'Votre fournisseur a été créé avec succès')
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='login')
def update(request, slug):
    print('sliug', slug)
    supplier = Supplier.objects.filter(slug = slug).first()
    print('supplier', supplier)
    if not request.user.is_superuser or not supplier:
        print('here 1')
        messages.success(request, 'Opps :>')
        return redirect('login')
    if request.method == 'POST':
        print('here 2')
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        supplier.name = name
        supplier.email = email
        supplier.phone = phone
        supplier.save()
        messages.success(request, 'Votre fournisseur a été modifié avec succès')
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='login')
def delete(request, slug):
    supplier = Supplier.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not delete:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    supplier.delete()
    messages.success(request, 'Deleted')
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='login')
def show(request, slug):
    supplier = Supplier.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not delete:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    products = Product.objects.filter(supplier = supplier).all()
    return render(request, 'supplier/show.html', {'supplier':supplier ,'products':products})


@login_required(login_url='login')
def list_products(request, slug):
    supplier = Supplier.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not delete:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    products = Product.objects.filter(supplier = supplier).all()
    return render(request, 'admincos/list/products.html', {'supplier':supplier ,'products':products})
