from .models import Supplier

def menu_suppliers(request):
    suppliers = Supplier.objects.all()
    return dict(suppliers=suppliers)
