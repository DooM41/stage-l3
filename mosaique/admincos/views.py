import datetime
from django.contrib import messages
from django.shortcuts import redirect, render
from supplier.models import Supplier
from category.models import Category
from store.models import Product
from django.db.models import Count
from django.contrib.auth.decorators import login_required
from orders.models import Order, OrderProduct
from .context_processors import getters




@login_required(login_url='login')
def controlepanel(request):
    orders = Order.objects.filter(is_active=True).all()
    return render(request, 'admincos/controlepanel.html', {"orders" : orders})

@login_required(login_url='login')
def archived_commandes(request):
    orders = Order.objects.filter(is_active=False).all()
    return render(request, 'admincos/controlepanel.html', {"orders" : orders})

@login_required(login_url='login')
def list_orders(request):
    return redirect('admincos:controlepanel')

@login_required(login_url='login')
def one_order(request, number):
    order = Order.objects.filter(order_number=number).first()
    if not request.user.is_superuser or not order:
        return redirect(request.META.get('HTTP_REFERER'))
    order.items = OrderProduct.objects.filter(order = order).all()
    return render(request, 'admincos/show/order.html', {"order":order})

@login_required(login_url='login')
def change_visibilty(request, number, vis):
    order = Order.objects.filter(order_number=number).first()
    if not request.user.is_superuser or not order or vis not in ['True', 'False']:
        return redirect(request.META.get('HTTP_REFERER'))
    res = True if vis=='True' else False
    order.is_active = res
    order.save()
    return render(request, 'admincos/show/order.html', {"order":order})


@login_required(login_url='login')
def edit_order(request, number):
    res = {
            1: 'Nouvelle commande',
            2: 'En cours de traitement',
            3: 'Commande prête',
            4: 'Commande récupéré',
            5: 'Commande annulée',
                }
    order = Order.objects.filter(order_number=number).first()
    if not request.user.is_superuser or not order:
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        order.status = res[int(request.POST.get('new-order-status'))]
        order.save()

    return redirect('admincos:one_order', order.order_number)


@login_required(login_url='login')
def list_supplier(request):
    suppliers = Supplier.objects.all()
    return render(request, 'admincos/list/supplier.html', {"suppliers": suppliers})



@login_required(login_url='login')
def products_list(request):
    # categories = Category.objects.all()
    products = Product.objects.all()
    return render(request, 'admincos/list/products.html', {"products": products})


@login_required(login_url='login')
def archived_products(request):
    products = Product.objects.filter(is_available=False).all()
    return render(request, 'admincos/list/products.html', {"products": products})



@login_required(login_url='login')
def create_product(request):
    if not request.user.is_superuser:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        product_name = request.POST.get('product_name')
        description = request.POST.get('description')
        price = request.POST.get('price')
        stock = request.POST.get('stock')
        category_id = request.POST.get('category')
        supplier_id = request.POST.get('supplier')
        category = Category.objects.filter(id = category_id).first()
        supplier = Supplier.objects.filter(id = supplier_id).first()
        if not category or not supplier:
            messages.success(request, 'Opps :>')
            return redirect(request.META.get('HTTP_REFERER'))
        product = Product.objects.create(
            product_name = product_name,
            description = description,
            price = price,
            stock = stock,
            category = category,
            supplier = supplier,
        )
        if request.FILES.get('product_image'):
            img = request.FILES.get('product_image')
            product.images = img
            product.save()
        # return redirect(request.META.get('HTTP_REFERER'))
        return redirect('admincos:one_produit', product.slug)

    return render(request, 'admincos/create/product.html')

@login_required(login_url='login')
def one_produit(request, slug):
    product = Product.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not product:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))

        # return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admincos/show/product_detail_admin.html', {'single_product': product})


@login_required(login_url='login')
def delete_produit(request, slug):
    product = Product.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not product:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    product.delete()
    return redirect('admincos:products_list')


@login_required(login_url='login')
def update_product(request, slug):
    product = Product.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not product:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        product_name = request.POST.get('product_name')
        description = request.POST.get('description')
        price = request.POST.get('price')
        stock = request.POST.get('stock')
        category_id = request.POST.get('category')
        supplier_id = request.POST.get('supplier')
        category = Category.objects.filter(id = category_id).first()
        supplier = Supplier.objects.filter(id = supplier_id).first()
        if not category or not supplier:
            messages.success(request, 'Opps :>')
            return redirect(request.META.get('HTTP_REFERER'))

        product.product_name = product_name
        product.description = description
        product.price = price
        product.stock = stock
        product.category = category
        product.supplier = supplier

        if request.FILES.get('product_image'):
            img = request.FILES.get('product_image')
            product.images = img

        product.save()
        return redirect('admincos:one_produit', product.slug)
    return render(request, 'admincos/update/product.html', {"product":product})




@login_required(login_url='login')
def create_supplier(request):
    if not request.user.is_superuser:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        email = request.POST.get('email')

        supplier = Supplier.objects.create(
            name = name,
            phone = phone,
            email = email,
        )
        return redirect('admincos:one_supplier', supplier.slug)
    return render(request, 'admincos/create/supplier.html')

@login_required(login_url='login')
def one_supplier(request, slug):
    supplier = Supplier.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not supplier:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    products = Product.objects.filter(supplier = supplier).all()
    return render(request, 'supplier/show.html', {'supplier': supplier, 'products':products})




#################################################################################
################################   category  ####################################
#################################################################################

@login_required(login_url='login')
def list_categories(request):
    categories = Category.objects.annotate(num_products=Count('product'))
    return render(request, 'admincos/list/categories.html', {"categories": categories})

@login_required(login_url='login')
def create_category(request):
    if not request.user.is_superuser:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        category_name = request.POST.get('category_name')
        description = request.POST.get('category_description')

        category = Category.objects.create(
            category_name = category_name,
            description = description,
        )
        if request.FILES.get('category_image'):
            img = request.FILES.get('category_image')
            category.cat_image = img
            category.save()
        messages.success(request, 'La catégorie a été créée avec succès')
        return redirect('admincos:one_category', category.slug)
    return render(request, 'admincos/create/category.html')

@login_required(login_url='login')
def one_category(request, slug):
    category = Category.objects.filter(slug = slug).first()
    if not request.user.is_superuser or not category:
        messages.success(request, 'Opps :>')
        return redirect(request.META.get('HTTP_REFERER'))
    # get nb produit for this category
    products = Product.objects.filter(category = category).all()
    return render(request, 'category/show.html', {'category': category, 'products':products})



from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template

from xhtml2pdf import pisa

def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None


from django.http import HttpResponse
from django.views.generic import View


def deneratePdf(request):
    data = {
            'today': datetime.date.today(),
            'amount': 39.99,
        'customer_name': 'Cooper Mann',
        'order_id': 1233434,
    }
    pdf = render_to_pdf('pdf/invoice.html', data)
    return HttpResponse(pdf, content_type='application/pdf')
