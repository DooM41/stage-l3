from django.apps import AppConfig


class AdmincosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admincos'
