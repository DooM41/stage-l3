from django.urls import path
from . import views

app_name='admincos'

urlpatterns = [
    path('controle-panel', views.controlepanel, name='controlepanel'),


    #################################################################################
    ##################################   order  #####################################
    #################################################################################
    path('orders/list', views.list_orders, name='list_orders'),
    path('orders/archived/list', views.archived_commandes, name='archived_commandes'),
    path('order/one/<int:number>/show', views.one_order, name='one_order'),
    path('order/one/<int:number>/edit', views.edit_order, name='edit_order'),
    path('order/one/<int:number>/set-visibilty/<str:vis>', views.change_visibilty, name='change_visibilty'),




    #################################################################################
    ################################   supplier  ####################################
    #################################################################################
    path('supplier/create', views.create_supplier, name="create_supplier"),
    path('supplier/list', views.list_supplier, name='list_supplier'),
    path('supplier/<slug:slug>/show', views.one_supplier, name="one_supplier"),





    #################################################################################
    ################################   category  ####################################
    #################################################################################
    path('category/create', views.create_category, name="create_category"),
    path('category/list', views.list_categories, name='list_categories'),
    path('category/<slug:slug>/show', views.one_category, name="one_category"),




    #################################################################################
    #################################   product  ####################################
    #################################################################################
    path('product/create', views.create_product, name="create_product"),
    path('product/<slug:slug>/update', views.update_product, name="update_product"),
    path('products/list', views.products_list, name='products_list'),
    path('product/<slug:slug>/show', views.one_produit, name="one_produit"),
    path('product/<slug:slug>/delete', views.delete_produit, name="delete_produit"),
    path('products/archived/list', views.archived_products, name='archived_products'),

]