from orders.models import Order

def getters(request):
    res = {
        1: 'Nouvelle commande',
        2: 'En cours de traitement',
        3: 'Commande prête',
        4: 'Commande récupéré',
        5: 'Commande annulée',
    }
    return {
        'get_all_order_status' : res,
    }
